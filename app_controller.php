<?php
/**CakePHP 1.3 > Altered by blAC**/

class AppController extends Controller {
	var $helpers = array(
		'Html', 
		'Form', 
		'Session',
		'Media',
	);

	var $components = array(
		'Session',
		'RequestHandler',
		//Add a comment to the next line to de-activate the Auth component
		'Auth'
	);


	function beforeFilter(){
	
		//Allow access for adding a person if the people controller is called
		if($this->Auth && $this->name == 'People' || $this->name == 'Users'):
			$this->Auth->allow('add');
		
		endif;
		
		if(isset($this->Auth) && App::import('Model', 'Person')):
		
			$this->loadModel('Person');
		
			//also send a trigger to the view to allow unhiding of people-related features
			$this->set('show_people_features', 1);
		
		endif;
		
		
		if(!$this->RequestHandler->isAjax()):
			//Generic Naming patterns base on containing folder name
			//Change here manually if you please
			$appName = @$_SESSION['appName'];
			$tagLine = @$_SESSION['tagLine'];
			$poster  = @$_SESSION['poster'];

			//Set authenticated user data if it is available
			$user = $this->_setUser();
		
			//Send the vars to the view
			$this->set(compact('appName', 'tagLine', 'poster', 'user'));
		endif;

		//Load Addendums
		$this->get_addendums();
		
		/**START CUSTOM CODE HERE**/


	}


/******===============
*******===============
***blAC METHODS=======
======================
======================
====================*/
	public function get_addendums(){
	
		/**Controller Addendums**/
		$addController = $this->name."ControllerAddendum";
		$add_controller_file = inflector::tableize($this->name)."_controller_addendum.php";
		$file_and_path = APP."libs".DS."addendums".DS.$add_controller_file;
	
		@include($file_and_path);
	
		if (class_exists($addController)) {
			$addendum = new $addController;
			$add_methods = get_class_methods($addController);
			$add_vars = get_class_vars($addController);
	
			//Apply the addendum vars to $this
			foreach($add_vars as $key=>$value):
			if(!isset($this->$key)):
			$this->$key = $value;
			endif;
			endforeach;
	
			//Apply the addendum methods to this
			##must access methods directly and not with $this
			$func = $this->params['action'];
	
			//set up addendum method for the specific call
			$am = $func."_add";
			if(method_exists($addendum, $am)):
			//run the addendum method if exists
			$addendum::$am();
			else:
			$this->Session->setFlash('No addendum');
			endif;
	
		}
	}

	//If the Auth component is used find the authenticated user data and return it
	private function _setUser(){
		if(isset($this->Auth)):

			//tell the view that the AuthStatus bar will be used
			$this->set('AuthStatus', 1);

			//get the auth user data
			$u = @$this->Auth->User();
			if(is_array($u)):
				return @$u['User'];
			
			endif;


		else:
			//tell the view that the AuthStatus bar will not be used
			$this->set('AuthStatus', 0);
			if($this->params['action']=='login'):
				$this->Session->setFlash('This application does not require authentication');
				$this->redirect(array('controller' => 'pages', 'action' => 'display', 'home'));
			endif;
		endif;
	}

	public function deactivate($model = null, $record = null, $status = 0){
		//simple polymorphic f(x) to deactivate or activate a field
		$this->loadModel($model);
		$this->$model->read(null, $record);
		$this->$model->set("Active",$status);
		if($this->$model->save()){
			return true;	
		}else{
			return false;
		}

	}
	/**Trial inclusion into blAC though it might be too specific**/
	public function get_user_for_list($model = null, $model_id = null){
		$this->autoRender = false;
		$this->layout = 'ajax';
		if($this->RequestHandler->isAjax()){
			$this->loadModel($model);
			$find_person = $this->$model->find('list', array(
				'fields' => array($model.'.id', $model.'.person_id'),
				'conditions' => array($model.'.id' => $model_id),
				'recursive' => 0
			));
			$person = $find_person[$model_id];
			$this->loadModel('Person');
			$find_data = $this->Person->find('list', array(
				'fields' => array('id', 'FullName'),
				'conditions' => array('id' => $person),
				'recursive' => 0
			));
			$pdata = $find_data[$person];
			echo $pdata;
			
		}else{
			echo "Person not found";
		}		
	}

	function getAll($model = null, $filter_by = null, $id = null, $recursion = 2){
		$this->loadModel($model);
		$findall = $this->$model->find('all', array(
			'recursive' => $recursion,
			'conditions' => array(
				$model.'.'.$filter_by => $id
			)
		));
		
		
		if(!empty($findall)):
		
			//in this case when 2 is rendered the results are not filtered
			if($recursion == -3)://-3 is not real cake but used to get ID's only
				foreach($findall as $fa):
					$myarr[$fa[$model]['id']] = @$fa[$model]['id'];
				endforeach;		
			elseif($recursion != 2):
				foreach($findall as $fa):
					$myarr[$fa[$model]['id']] = @$fa[$model];
				endforeach;
			else:
				$myarr = $findall;
			endif;
			
			//return filtered results
			return $myarr;
			
		else:
			//no results found
			return array();
			
		endif;
	}
	
	
	function get($model = null, $rfield = null, $field = null, $val = null){

		$recursive = 2; //set to improve query ability
		if($model):

			//open the db model
			$this->loadModel($model);
			
			//ensure the field and requested field are valid
			if($field && !is_array($field)):

				
				$r = $this->$model->find('first', array(

					'recursive' => $recursive,

					'conditions' => array(
						//the field should be set to the value
						//like Name='James'
						$model.'.'.$field => $val

					),
					//the field that is to be returned can be any column in the table
					//like Birthday (will return James' birthday
					'fields' => $rfield
			
				));

			endif;

			return $r[$model][$rfield];


		endif;

	}
}
